#!/bin/bash

# Get the full path to this file and change to that directory
BUILD_PATH=$(cd ${0%/*} && pwd -P)
cd $BUILD_PATH

# Go to parent directory to do build
cd ..
gulp default
cd $BUILD_PATH

# do the cordova build
cordova build android --release

# Get the version
VERSION=`cat ../config.xml \
    | grep '^<widget' \
    | sed 's|^.*version="\([^"]*\)".*|\1|'`

echo ""
echo "Signing IIIPoints version: $VERSION"


# copy unsigned apk to this directory
cp ../platforms/android/build/outputs/apk/android-release-unsigned.apk .

# sign it
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore iiipoints-release.keystore android-release-unsigned.apk iiipoints

# optimize it
rm IIIPoints-$VERSION.apk
~/Library/Android/sdk/build-tools/25.0.0/zipalign -v 4 android-release-unsigned.apk IIIPoints-$VERSION.apk