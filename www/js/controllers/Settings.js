;(function () {
  'use strict'

  angular
    .module('iiipoints.controllers')
    .controller('SettingsCtrl', SettingsCtrl)

  SettingsCtrl.$inject = ['$scope', '$state']
  function SettingsCtrl ($scope, $state) {
    var vm = this

    vm.showLogin = showLogin;
    vm.showNotificationSettings = showNotificationSettings;
    vm.showLocationsSettings = showLocationsSettings;
    vm.showMessageCenter = showMessageCenter;

    activate()

    // //////////////

    function activate () { }

    function showLogin() { 
      // $scope.$parent.openLogin();
      $state.go('app.login');
    }

    function showNotificationSettings() { 
      $scope.$parent.openNotificationsModal();
    }

    function showLocationsSettings() {
      $scope.$parent.openLocationModal();
    }

    function showMessageCenter() { 
      document.addEventListener("deviceready", function () {
        UAirship.displayMessageCenter();
      }, false);
    }
  }
})()
