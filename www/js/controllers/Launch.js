(function () {
  'use strict';

  angular
    .module('iiipoints.controllers')
    .controller('LaunchCtrl', LaunchCtrl);

  LaunchCtrl.$inject = ['$scope', '$state', '$cordovaInAppBrowser']
  function LaunchCtrl ($scope, $state, $cordovaInAppBrowser) {
    var vm = this;

    activate();

    ////////////////

    function activate () { }

    $scope.goHome = function(url) {
      $state.go('home');
    }

    $scope.launch = function(url) {
      document.addEventListener("deviceready", function () {
        var options = {
          location: 'yes',
          clearcache: 'no',
          toolbar: 'yes'
        };

        $cordovaInAppBrowser.open(url, '_blank', options)
          .then(function(event) {
            // success
          })
          .catch(function(event) {
            // error
          });
      }, false);
    }

  }

})();
