(function() {
'use strict';

  angular
    .module('iiipoints.controllers')
    .controller('WelcomeCtrl', WelcomeCtrl);

  WelcomeCtrl.$inject = ['$scope', '$state', '$ionicHistory', 'FestopiaService', 'Authentication', 'Utils'];
  function WelcomeCtrl($scope, $state, $ionicHistory, FestopiaService, Authentication, Utils) {
    var vm = this;

    var modalList = [];

    activate();
    ////////////////

    function activate() { }

    $scope.$on("$ionicView.enter", function(event, data){
      // handle event
      autoLoginAndInitialDataFetch();
    });

    function autoLoginAndInitialDataFetch() {
      Utils.showLoading();
      var userCredentials = FestopiaService.getUserCredentials();
      Authentication.login(userCredentials.username, userCredentials.password)
        .then(function (res) {
          Utils.hideLoading();
          dismissSplash();
        }, function (err) {
          Utils.hideLoading();
          console.log("Error Logging In:", err);
          dismissSplash();
        });
    }

    function loadInitialData() {
      Utils.hideLoading();
      // check to dismiss splash
      // if (!FestopiaService.checkToShowSettingsPopup('SEEN_LOCATION_SETTINGS')) {
      //   $scope.$parent.openLocationModal();
      // }
      if (!FestopiaService.checkToShowSettingsPopup('SEEN_NOTIFICATION_SETTINGS')) {
        $scope.$parent.openNotificationsModal();
      }
      // else if (!FestopiaService.checkToShowSettingsPopup('SEEN_CONNECT_SETTINGS')) {
      //   $scope.$parent.openConnectModal();
      // }
      else {
        dismissSplash();
      }
    }

    function dismissSplash() {
      // Go to "launch" screen instead of "home"
      $state.go('app.home');
      $ionicHistory.nextViewOptions({
        historyRoot: true
      });
    }

  }
})();
