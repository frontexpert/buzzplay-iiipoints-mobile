(function() {
  'use strict';
  
    angular
      .module('iiipoints.controllers')
      .controller('SponsorsCtrl', SponsorsCtrl);
  
    SponsorsCtrl.$inject = ['SponsorsFactory', 'Utils', '$cordovaInAppBrowser'];
    function SponsorsCtrl(SponsorsFactory, utils, $cordovaInAppBrowser) {
      var vm = this;
      
      vm.openWebsite = openWebsite;
      vm.sponsors = [];
  
      activate();
  
      ////////////////
  
      function activate() { 
        utils.showLoading();
        // get sponsors
        SponsorsFactory.getSponsors()
          .then(function(resp) {
            vm.sponsors = resp.data;
            utils.hideLoading();
          });
      }

      function openWebsite (url) {
        var options = {
          location: 'yes',
          clearcache: 'no',
          toolbar: 'yes'
        };
  
        document.addEventListener('deviceready', function () {
          $cordovaInAppBrowser.open(url, '_system', options)
            .then(function (event) {
              // success
            })
            .catch(function (event) {
              // error
            })
  
        }, false)
      }
    }
  })();