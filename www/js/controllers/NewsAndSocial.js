(function() {
'use strict';

  angular
    .module('iiipoints.controllers')
    .controller('NewsAndSocialCtrl', NewsAndSocialCtrl);

  NewsAndSocialCtrl.$inject = ['$cordovaInAppBrowser'];
  function NewsAndSocialCtrl($cordovaInAppBrowser) {
    var vm = this;

    vm.launch = launch;
    vm.instegramUrl =  "https://www.instagram.com/iiipoints/";
    vm.twitterUrl   = "https://twitter.com/iiipoints";
    vm.facebookUrl  = "https://www.facebook.com/iiipoints";
    vm.options = {
      location: 'yes',
      clearcache: 'no',
      toolbar: 'yes'
    };

    activate();

    ////////////////

    function activate() { }

    function launch(url) {
      document.addEventListener("deviceready", function () {
        $cordovaInAppBrowser.open(url, '_system', vm.options)
          .then(function(event) {
            // success
          })
          .catch(function(event) {
            // error
          });
      }, false);
    }
  }
})();