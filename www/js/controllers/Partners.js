(function() {
'use strict';

  angular
    .module('iiipoints.controllers')
    .controller('PartnersCtrl', PartnersCtrl);

  
  PartnersCtrl.$inject = ['$scope','$cordovaInAppBrowser'];
  function PartnersCtrl($scope,$cordovaInAppBrowser) {
    var vm = this;
    var options = {
      location: 'yes',
      clearcache: 'no',
      toolbar: 'yes'
    };
    vm.launch = launch;
    vm.openWebsite=openWebsite;
    $scope.groups=[];
    activate();
    ////////////////
    function activate() { }

    $scope.groups=[
      {name:"GENERAL",items:["img/general.png"],show:false},
      {name:"TICKETS/BOX OFFICE",items:["img/ticketbox.png"],show:false},
      {name:"TRANSPORTATION/PARKING",items:["img/transportation.png"],show:false},
      {name:"WHAT NOT TO BRING",items:["img/whatnottobring.png"],show:false},
      {name:"INQUIRIES",items:["img/inquires.png"],show:false},
      {name:"ADA ACCESS",items:["img/adaaccess.png"],show:false}

    ];
     
     $scope.toggleGroup = function(group) {
      group.show = !group.show;
    };
    $scope.isGroupShown = function(group) {
      return group.show;
    };

    function launch(url) {
      if (url === 'N/A' || url === undefined || url === null) {
        Utils.showAlert('There is no the web page at Url.');
        return;
      }

      document.addEventListener("deviceready", function () {
        $cordovaInAppBrowser.open(url, '_blank', options)
          .then(function(event) {
            // success
          })
          .catch(function(event) {
            // error
          });
      }, false);
      
    }

    function openWebsite (url, top) {
      var options = {
        location: 'yes',
        clearcache: 'no',
        toolbar: 'yes'
      };

      document.addEventListener('deviceready', function () {
        $cordovaInAppBrowser.open(url, top || '_system', options)
          .then(function (event) {
            // success
          })
          .catch(function (event) {
            // error
          })

      }, false)
    }

  }
})();