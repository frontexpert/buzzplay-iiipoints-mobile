;(function () {
  'use strict'

  angular
    .module('iiipoints.controllers', [])
    .controller('AppMenuCtrl', AppMenuCtrl)

  AppMenuCtrl.$inject = ['$scope', '$state', '$ionicModal', '$timeout', '$location', '$cordovaInAppBrowser', 'FestopiaService', '$ionicHistory']
  function AppMenuCtrl ($scope, $state, $ionicModal, $timeout, $location, $cordovaInAppBrowser, FestopiaService, $ionicHistory) {
    var vm = this

    vm.goHome = goHome;
    vm.showMessageCenter = showMessageCenter
    vm.openWebsite = openWebsite
    vm.options = {
      location: 'yes',
      clearcache: 'no',
      toolbar: 'yes'
    }

    $scope.userLocationEnabled = true;
    $scope.bluetoothEnabled = true;
    $scope.allowNotifications = true;

    activate()

    // //////////////

    function activate () {
    }

    function goHome() {

      $location.path("/app/home");

      $ionicHistory.nextViewOptions({
        historyRoot: true
      });

    }

    function showMessageCenter () {

      // Dirty hack to get the message center to show from the side menu.
      UAirship.displayMessageCenter()

    }

    function openWebsite (url) {
      document.addEventListener('deviceready', function () {
        $cordovaInAppBrowser.open(url, '_system', vm.options)
          .then(function (event) {
            // success
          })
          .catch(function (event) {
            // error
          })

      }, false)
    }

    // location modal
    $ionicModal.fromTemplateUrl('templates/location.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.locationModal = modal;
    });

    $scope.openLocationModal = function() {

      $scope.locationModal.show();

    };

    $scope.closeLocationModal = function() {

      // set it to seen
      FestopiaService.setSeenLocationsSettings();

      $scope.locationModal.hide();

      if ( !FestopiaService.checkToShowSettingsPopup('SEEN_NOTIFICATION_SETTINGS') ) {
        $timeout(function() {
          $scope.openNotificationsModal();
        }, 1500);
      }

    };

    // Notifications modal
    $ionicModal.fromTemplateUrl('templates/notifications.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.notificationModal = modal;
    });

    $scope.openNotificationsModal = function() {

      $scope.notificationModal.show();

    };

    $scope.closeNotificationsModal = function() {

      // set it to seen
      FestopiaService.setSeenNotificationsSettings();

      if (ionic.Platform.isAndroid() || ionic.Platform.isIOS()) {

        document.addEventListener("deviceready", function () {
          FestopiaService.regsiterDeviceForPush($scope.allowNotifications)
          .then(function(res) {
            $scope.notificationModal.hide();
          });
        }, false);

      }
      else {

        $scope.notificationModal.hide();

      }

      // go home
      $timeout(function() {

        goHome();

      }, 1500);

      // if ( !FestopiaService.checkToShowSettingsPopup('SEEN_CONNECT_SETTINGS') ) {
      //   $timeout(function() {
      //     $scope.openConnectModal();
      //   }, 1500);
      // }

    };

    // connect modal
    $ionicModal.fromTemplateUrl('templates/connect.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.connectModal = modal;
    });

    $scope.openConnectModal = function() {
      $scope.connectModal.show();
    };

    $scope.closeConnectModal = function() {

      FestopiaService.setSeenConnectSettings();

      // hide modal
      $scope.connectModal.hide();

      $timeout(function() {

        goHome();

      }, 1500);

    };

    $scope.showLogin = function() {

      $scope.connectModal.hide();

      // go login screen
      $state.go('app.login');

    }
  }
})()
