(function() {
'use strict';

  angular
    .module('iiipoints.controllers')
    .controller('RegisterCtrl', RegisterCtrl);

  RegisterCtrl.$inject = ['$state', 'Authentication', 'Utils'];
  function RegisterCtrl($state, Authentication, Utils) {
    var vm = this;
    
    vm.doRegister = doRegister;
    vm.userData = {};

    activate();

    ////////////////

    function activate() { }

    function doRegister(form) {
      if ( !form.$valid ) {
        return;
      }

      if (vm.userData) {
        Utils.showLoading();

        Authentication.createUser(vm.userData)
          .then(function(res) {
            Utils.hideLoading();
            // goto landing page
            $state.go('schedule');
          }, function(err) {
            Utils.hideLoading();
            console.log("Error Creating Account In:", err);
          })
      }
    }
  }
})();