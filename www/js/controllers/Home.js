(function() {
'use strict';

  angular
    .module('iiipoints.controllers')
    .controller('HomeCtrl', HomeCtrl);

  HomeCtrl.inject = ['$state', '$cordovaInAppBrowser', 'APP_SOCIAL_LINKS'];
  function HomeCtrl($state, $cordovaInAppBrowser, APP_SOCIAL_LINKS) {
    var vm = this;

    vm.goSettingsPage = goSettings;
    vm.onBuyTix = onBuyTix;
    vm.onShowSnapChat = onShowSnapChat;
    vm.onShowInstagram = onShowInstagram;
    vm.onShowTwitter = onShowTwitter;
    vm.onShowFacebook = onShowFacebook;
    vm.onShowSoundcloud = onShowSoundcloud;
    vm.goSchedulePage = goSchedulePage;
    vm.goMapPage = goMapPage;
    vm.onShowSponsorsPage = onShowSponsorsPage;
    vm.onShowArtistPage = onShowArtistPage;
    vm.openWebsite = openWebsite;
    vm.onShowFaqPage=onShowFaqPage;

    activate();

    ////////////////

    function activate() { }

    function goSettings () {
      $state.go('app.settings');
    }

    function onBuyTix() {
      $state.go('app.external', {type: 'buyTix'});
    }

    function openWebsite (url, top) {
      var options = {
        location: 'yes',
        clearcache: 'no',
        toolbar: 'yes'
      };

      document.addEventListener('deviceready', function () {
        $cordovaInAppBrowser.open(url, top || '_system', options)
          .then(function (event) {
            // success
          })
          .catch(function (event) {
            // error
          })

      }, false)
    }

    function onShowSnapChat() {
      // $state.go('external', {type: 'snapchat'});
      openWebsite(APP_SOCIAL_LINKS.SNAPCHAT, '_system');
    }

    function onShowInstagram() {
      // $state.go('app.external', {type: 'instagram'});
      openWebsite(APP_SOCIAL_LINKS.INSTAGRAM);
    }

    function onShowTwitter() {
      // $state.go('app.external', {type: 'twitter'});
      openWebsite(APP_SOCIAL_LINKS.TWITTER);
    }

    function onShowFacebook() {
      // $state.go('app.external', {type: 'facebook'});
      openWebsite(APP_SOCIAL_LINKS.FACEBOOK);
    }

    function onShowSoundcloud() {
      $state.go('app.external', {type: 'soundcloud'});
    }

    function goSchedulePage() {
      $state.go('schedule.all');
    }

    function goMapPage() {
      $state.go('app.map');
    }

    function onShowArtistPage() {
      $state.go('app.artists');
    }

    function onShowSponsorsPage() {
      $state.go('app.sponsors');
    }
    function onShowFaqPage() {
      $state.go('app.partners');
      
    }
  }
})();
