(function() {
'use strict';

  angular
    .module('iiipoints.controllers')
    .controller('MapCtrl', MapCtrl);

  MapCtrl.$inject = ['$scope', 'FestopiaService', 'Utils', 'APP_CONFIG'];
  function MapCtrl($scope, FestopiaService, Utils, APP_CONFIG) {
    var vm = this;

    vm.markers = [];
    vm.map = null;

    activate();

    ////////////////

    function activate() {

      // loadGoogleMaps();
      loadImage();

    }

    function loadImage() {
      $scope.imgUrl = APP_CONFIG.MAP_IMAGE_URL;
    }

    $scope.$on('$ionicView.enter', function(){
      screen.orientation.lock('landscape');
    });

    $scope.$on('$ionicView.leave', function(){
      screen.orientation.unlock();
    });

    function loadGoogleMaps(){

      Utils.showLoading();

      //This function will be called once the SDK has been loaded
      window.mapInit = function(){
        initMap();
      };

      //Create a script element to insert into the page
      var script = document.createElement("script");
      script.type = "text/javascript";
      script.id = "googleMaps";

      script.src = 'http://maps.google.com/maps/api/js?key=' + APP_CONFIG.MAP_API_KEY
        + '&callback=mapInit';

      document.body.appendChild(script);

    }

    function initMap() {
      var myLatlng = new google.maps.LatLng(25.84001,-80.190158);

      var mapOptions = {
        center: myLatlng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      var map = new google.maps.Map(document.getElementById("map"),
          mapOptions);

      //Wait until the map is loaded
      google.maps.event.addListenerOnce(map, 'idle', function(){
        Utils.hideLoading();

        loadMarkers();
      });

      vm.map = map;

    }

    function loadMarkers(){
      Utils.showLoading();

      //Get all of the markers from our Markers factory
      FestopiaService.fetchAllPOI().then(function(venues){
        Utils.hideLoading();

        console.log("Markers: ", venues);

        for (var i = 0; i < venues.length; i++) {

          var venue = venues[i];
          var markerPos = new google.maps.LatLng(venue['_geoloc/0'], venue['_geoloc/1']);

          // Add the markerto the map
          var marker = new google.maps.Marker({
              map: vm.map,
              animation: google.maps.Animation.DROP,
              position: markerPos
          });

          var infoWindowContent = "<h4 style='color: #000;'>" + venue.name + "</h4>"
            + "<h5 style='color: #000;'>"+ venue.address + "</h5>";

          addInfoWindow(marker, infoWindowContent, venue);

        }

      });

    }

    function addInfoWindow(marker, message, venue) {

      var infoWindow = new google.maps.InfoWindow({
          content: message
      });

      google.maps.event.addListener(marker, 'click', function () {
          infoWindow.open(vm.map, marker);
      });

    }
  }
})();
