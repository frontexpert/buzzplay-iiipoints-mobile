;(function () {
  'use strict'

  angular
    .module('iiipoints.controllers')
    .controller('LoginCtrl', LoginCtrl)

  LoginCtrl.$inject = ['FestopiaService', 'Authentication', 'Utils']
  function LoginCtrl (FestopiaService, Authentication, Utils) {
    var vm = this

    vm.doLogin = doLogin
    vm.loginData = null

    activate()

    // //////////////

    function activate () {
      console.log('login page...');
    }

    function doLogin (form) {
      if ( !form.$valid ) {
        return;
      }

      console.log(vm.loginData)
      if (vm.loginData) {
        Utils.showLoading();

        Authentication.login(vm.loginData.username, vm.loginData.password)
          .then(function (res) {
            Utils.hideLoading();
            console.log('Login Success', res)
            FestopiaService.saveUserCredentials(vm.loginData.username, vm.loginData.password)
              .then(function (res) {
                // close login go back
                Utils.goBack();
              })
          })
          .catch(function (err) {
            Utils.hideLoading();
            console.log(err)
          })
      }
    }
  }
})();