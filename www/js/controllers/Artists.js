;(function () {
  'use strict'

  angular
    .module('iiipoints.controllers')
    .controller('ArtistsCtrl', ArtistsCtrl)
    .filter('convertTime', convertTimeFormat)

  ArtistsCtrl.$inject = ['$state', '$cordovaInAppBrowser', 'Utils', 'ArtistsFactory']
  function ArtistsCtrl ($state, $cordovaInAppBrowser, utils, artistsFactory) {
    var vm = this;
    vm.goBackToHome = goBackToHome;
    vm.showProfileDetails = showProfileDetails;
    vm.openWeb = openWebsite;
    vm.setFavorite = setFavorite;
    vm.options = {
      location: 'yes',
      clearcache: 'no',
      toolbar: 'yes'
    };
    vm.artists = [];

    activate()

    // //////////////

    function activate () {
      utils.showLoading();

      // get artists by day
      artistsFactory.getArtists()
        .then(function (response) {
          console.log('artists: ', response);
          vm.artists = _.orderBy(response, 'priority', 'desc');
          // vm.artists = _.chain(response.data)
          //   .groupBy("event_day")
          //   .toPairs()
          //   .map(function(currentItem) {
          //     return _.zipObject(['day', 'artists'], currentItem);
          //   })
          //   .value();
          // console.log(vm.artists);

          utils.hideLoading();

        });
    }

    function openWebsite (url) {
      document.addEventListener('deviceready', function () {
        $cordovaInAppBrowser.open(url, '_blank', vm.options)
          .then(function (event) {
            // success
          })
          .catch(function (event) {
            // error
          });

      }, false);
    }

    function goBackToHome() {
      utils.goBack();
    }

    function showProfileDetails(aId) {
      $state.go('app.profile', { id: aId});
    }

    function setFavorite(artist) {
      artist.favorite = !artist.favorite;
      artistsFactory.setFavorite(artist.id, artist.favorite);
    }
  }

  function convertTimeFormat() {
    return function(timeString) {
      var timeComponents = timeString.split(':');
      if (timeComponents[0] > 12)
        timeComponents[0] -= 12;
      return timeComponents[0] + ':' + timeComponents[1];
    }
  }
})()
