;(function () {
  'use strict'

  angular
    .module('iiipoints.controllers')
    .controller('CustomScheduleCtrl', CustomScheduleCtrl)

  CustomScheduleCtrl.$inject = ['$state', '$scope', '$cordovaInAppBrowser', 'Utils', 'ArtistsFactory']
  function CustomScheduleCtrl ($state, $scope, $cordovaInAppBrowser, utils, artistsFactory) {
    var vm = this;

    vm.goBackToHome = goBackToHome;
    vm.goSettingsPage = goSettings;
    vm.openWeb = openWebsite;
    vm.showProfileDetails = showProfileDetails;
    vm.chosenDay = null;
    vm.options = {
      location: 'yes',
      clearcache: 'no',
      toolbar: 'yes'
    };
    vm.artists = {};
    vm.sortBy = '';
    vm.setFavorite = setFavorite;

    $scope.$on('$ionicView.enter', function(){
      activate();
    });

    function activate () { 
      var _artists = artistsFactory.getFavoriteArtists();
      var artists = _.chain(_artists)
        .groupBy("event_day")
        .toPairs()
        .map(function(currentItem) {
          currentItem[1] = _sortByTime(currentItem[1]);
          return _.zipObject(['day', 'artists'], currentItem);
        })
        .value();
      artists.map(function(artistsGroup) {
        vm.artists[artistsGroup.day] = artistsGroup.artists;
      });
      console.log(vm.artists);
    }

    function goSettings () {
      $state.go('app.settings');
    }

    function openWebsite (url) {
      document.addEventListener('deviceready', function () {
        $cordovaInAppBrowser.open(url, '_blank', vm.options)
          .then(function (event) {
            // success
          })
          .catch(function (event) {
            // error
          });

      }, false);
    }

    function goBackToHome() {
      $state.go('app.home');
    }

    function showProfileDetails(aId) {
      $state.go('app.profile', { id: aId});
    }

    function _sortByTime(arr) {
      arr.forEach(function(item) {
        var t = item.performer_start_time.split(':');
        item.performer_start_time1 = t[0] * 3600 + t[1] * 60 + t[2] * 1;
        if(item.performer_start_time1 < 43200)
          item.performer_start_time1 += 86400;
      });

      arr = arr.sort(function(x, y) {
        if(x.performer_start_time1 < y.performer_start_time1)
          return -1;
        else if(x.performer_start_time1 > y.performer_start_time1)
          return 1;
        else
          return 0;
      });

      arr = _.orderBy(arr, ['performer_start_time1'], 'desc');

      return arr;
    }

    function setFavorite(artist) {
      artistsFactory.setFavorite(artist.id, false);
      activate();
    }
  }
})();
