(function() {
'use strict';

  angular
    .module('iiipoints.controllers')
    .controller('ExternalCtrl', ExternalCtrl);

  ExternalCtrl.inject = ['$state', '$stateParams', '$sce', 'APP_SOCIAL_LINKS', 'Utils'];
  function ExternalCtrl($state, $stateParams, $sce, APP_SOCIAL_LINKS, Utils) {
    var vm = this;

    vm.trustSrc = trustSrc;
    vm.goBackToHome = goBackToHome;
    vm.iframeURL = '';

    activate();

    ////////////////

    function activate() { 
      switch ($stateParams.type) {
        case 'buyTix':
          vm.iframeURL = APP_SOCIAL_LINKS.BUY_TIX;
        break;
        case 'snapchat':
          vm.iframeURL = APP_SOCIAL_LINKS.SNAPCHAT;
        break;
        case 'facebook':
          vm.iframeURL = APP_SOCIAL_LINKS.FACEBOOK;
        break;
        case 'instagram':
          vm.iframeURL = APP_SOCIAL_LINKS.INSTAGRAM;
        break;
        case 'twitter':
          vm.iframeURL = APP_SOCIAL_LINKS.TWITTER;
        break;
        case 'soundcloud':
          vm.iframeURL = APP_SOCIAL_LINKS.SOUNDCLOUD;
        break;
      }
    }

    function trustSrc (src) {
      return $sce.trustAsResourceUrl(src);
    }

    function goBackToHome() {
      Utils.goBack();
    }
  }
})();