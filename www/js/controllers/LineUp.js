(function() {
'use strict';

  angular
    .module('iiipoints.controllers')
    .controller('LineUpCtrl', LineUpCtrl);

  LineUpCtrl.$inject = ['FestopiaService', 'Utils'];
  function LineUpCtrl(FestopiaService, Utils) {

    var vm = this;
    
    vm.bands = {};

    activate();

    ////////////////

    function activate() { 
      loadBands();
    }

    function loadBands() {

      Utils.showLoading();

      FestopiaService.fetchAllBands()
        .then(function (bands) {

          Utils.hideLoading();
          vm.bands = bands;
          console.log(bands);

        }, function (err) {

          Utils.hideLoading();
          console.log(err);

        });
    }
    
  }
})();