(function() {
'use strict';

  angular
    .module('iiipoints.controllers')
    .controller('ProfileCtrl', ProfileCtrl);

  ProfileCtrl.$inject = ['$stateParams', 'ArtistsFactory', 'Utils', '$cordovaInAppBrowser'];
  function ProfileCtrl($stateParams, ArtistsFactory, Utils, $cordovaInAppBrowser) {

    var vm = this;
    var options = {
      location: 'yes',
      clearcache: 'no',
      toolbar: 'yes'
    };
    
    vm.launch = launch;
    vm.goBack = goBack;
    vm.profile = null;
    vm.openWebsite = openWebsite;
    vm.onShowSnapChat = onShowSnapChat;
    vm.onShowInstagram = onShowInstagram;
    vm.onShowTwitter = onShowTwitter;
    vm.onShowFacebook = onShowFacebook;
    vm.onShowSoundcloud = onShowSoundcloud;
    vm.setFavorite = setFavorite;
    activate();

    ////////////////

    function activate() { 
      var artistId = $stateParams['id'];
      console.log('Profile ID :', artistId);


      // get profile from id
      ArtistsFactory.getArtist(artistId)
        .then(function(artist) {
          console.log(artist);
          vm.profile = artist;
        });
    }

    function launch(url) {
      if (url === 'N/A' || url === undefined || url === null) {
        Utils.showAlert('There is no the web page at Url.');
        return;
      }

      document.addEventListener("deviceready", function () {
        $cordovaInAppBrowser.open(url, '_blank', options)
          .then(function(event) {
            // success
          })
          .catch(function(event) {
            // error
          });
      }, false);
      
    }

    /**
     * Back to previous page
     */
    function goBack() {
      Utils.goBack();
    }
    function openWebsite (url, top) {
      var options = {
        location: 'yes',
        clearcache: 'no',
        toolbar: 'yes'
      };

      document.addEventListener('deviceready', function () {
        $cordovaInAppBrowser.open(url, top || '_system', options)
          .then(function (event) {
            // success
          })
          .catch(function (event) {
            // error
          })

      }, false)
    }
    function onShowSnapChat() {
      // $state.go('external', {type: 'snapchat'});
      openWebsite(APP_SOCIAL_LINKS.SNAPCHAT, '_system');
    }

    function onShowInstagram() {
      // $state.go('app.external', {type: 'instagram'});
      openWebsite(APP_SOCIAL_LINKS.INSTAGRAM);
    }

    function onShowTwitter() {
      // $state.go('app.external', {type: 'twitter'});
      openWebsite(APP_SOCIAL_LINKS.TWITTER);
    }

    function onShowFacebook() {
      // $state.go('app.external', {type: 'facebook'});
      openWebsite(APP_SOCIAL_LINKS.FACEBOOK);
    }

    function onShowSoundcloud() {
      $state.go('app.external', {type: 'soundcloud'});
    }

    function setFavorite() {
      vm.profile.favorite = !vm.profile.favorite;
      ArtistsFactory.setFavorite(vm.profile.id, vm.profile.favorite);
    }
  }
})();