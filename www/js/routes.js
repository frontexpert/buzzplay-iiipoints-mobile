;(function () {
  'use strict'

  angular.module('iiipoints')
    .config(function ($stateProvider, $urlRouterProvider) {
      $stateProvider
        .state('app', {
          url: '/app',
          abstract: true,
          templateUrl: 'templates/menu.html',
          controller: 'AppMenuCtrl',
          controllerAs: 'vm'
        })
        .state('welcome', {
          url: '/welcome',
          templateUrl: 'templates/welcome.html',
          controller: 'WelcomeCtrl',
          controllerAs: 'vm'
        })
        // .state('app.settings', {
        //   url: '/settings',
        //   views: {
        //     'menuContent': {
        //       templateUrl: 'templates/settings.html',
        //       controller: 'SettingsCtrl',
        //       controllerAs: 'vm'
        //     }
        //   }
        // })
        .state('app.home', {
          url: '/home',
          views: {
            'menuContent': {
              templateUrl: 'templates/home.html',
              controller: 'HomeCtrl',
              controllerAs: 'vm'
            }
          }
        })
        .state('schedule', {
          url: "/schedule",
          abstract: true,
          templateUrl: "templates/schedules.html"
        })
        .state('schedule.all', {
          url: "/all",
          views: {
            'all-tab': {
              templateUrl: "templates/schedule.html",
              controller: 'ScheduleCtrl',
              controllerAs: 'vm'
            }
          }
        })
        .state('schedule.custom', {
          url: '/custom',
          views: {
            'custom-tab': {
              templateUrl: 'templates/schedule-custom.html',
              controller: 'CustomScheduleCtrl',
              controllerAs: 'vm'
            }
          }
        })
        .state('app.map', {
          url: '/map',
          views: {
            'menuContent': {
              templateUrl: 'templates/map.html',
              controller: 'MapCtrl',
              controllerAs: 'vm'
            }
          }
        })
        .state('app.artists', {
          url: '/artists',
          views: {
            'menuContent': {
              templateUrl: 'templates/artists.html',
              controller: 'ArtistsCtrl',
              controllerAs: 'vm'
            }
          }
        })

        .state('app.partners', {
          url: '/partners',
          views: {
            'menuContent': {
              templateUrl: 'templates/partners.html',
              controller: 'PartnersCtrl',
              controllerAs: 'vm'
            }
          }
        })

        .state('app.faq', {
          url: '/faq',
          views: {
            'menuContent': {
              templateUrl: 'templates/faq.html',
              controller: 'FaqCtrl',
              controllerAs: 'vm'
            }
          }
        })

        .state('app.profile', {
          url: '/profile/:id',
          views: {
            'menuContent': {
              templateUrl: 'templates/profile.html',
              controller: 'ProfileCtrl',
              controllerAs: 'vm'
            }
          }
        })
        .state('app.sponsors', {
          url: '/sponsors',
          views: {
            'menuContent': {
              templateUrl: 'templates/sponsors.html',
              controller: 'SponsorsCtrl',
              controllerAs: 'vm'
            }
          }
        })

        /*
        .state('app.lineup', {
          url: '/lineup',
          views: {
            'menuContent': {
              templateUrl: 'templates/line-up.html',
              controller: 'LineUpCtrl',
              controllerAs: 'vm'
            }
          }
        })
        .state('app.profile', {
          url: '/lineup/:bandID',
          views: {
            'menuContent': {
              templateUrl: 'templates/profile.html',
              controller: 'ProfileCtrl',
              controllerAs: 'vm'
            }
          }
        })
        .state('app.partners', {
          url: '/partners',
          views: {
            'menuContent': {
              templateUrl: 'templates/partners.html',
              controller: 'PartnersCtrl',
              controllerAs: 'vm'
            }
          }
        })
        .state('app.newsandsocial', {
          url: '/newsandsocial',
          views: {
            'menuContent': {
              templateUrl: 'templates/news-and-social.html',
              controller: 'NewsAndSocialCtrl',
              controllerAs: 'vm'
            }
          }
        })
        .state('app.login', {
          url: '/login',
          views: {
            'menuContent': {
              templateUrl: 'templates/login.html',
              controller: 'LoginCtrl',
              controllerAs: 'vm'
            }
          }
        })
        .state('app.register', {
          url: '/register',
          views: {
            'menuContent': {
              templateUrl: 'templates/register.html',
              controller: 'RegisterCtrl',
              controllerAs: 'vm'
            }
          }
        })*/
        .state('app.external', {
          url: '/external/:type',
          views: {
            'menuContent': {
              templateUrl: 'templates/external.html',
              controller: 'ExternalCtrl',
              controllerAs: 'vm'
            }
          }
        })
        .state('launch', {
          url: '/launch',
          templateUrl: 'templates/launch.html',
          controller: 'LaunchCtrl',
          controllerAs: 'vm'
        });

      // if none of the above states are matched, use this as the fallback
      $urlRouterProvider.otherwise('/welcome');
    })
})()
