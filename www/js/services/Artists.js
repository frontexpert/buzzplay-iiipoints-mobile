(function () {
  'use strict'

  angular
  	.module('iiipoints.services')
		.service('ArtistsFactory', ArtistsFactory);

	ArtistsFactory.$inject = ['$q', '$http', 'APP_CONFIG'];
	function ArtistsFactory($q, $http, APP_CONFIG) {
		var artists = [];

		var service = {
			getArtists: getArtists,
			getArtist: getArtist,
			setFavorite: setFavorite,
			getFavoriteArtists: getFavoriteArtists
		};

		return service;

		function getArtists() {
			var def = $q.defer();
			if (artists.length > 0) {
				def.resolve(artists);
			} else {
				$http.get(APP_CONFIG.API_BASE_URL + 'artists')
				.then(function (response) {
					artists = response.data.data;
					def.resolve(artists);
				}, function (error) {
					def.reject(error);
				});
			}
			return def.promise;
		}

		function getArtist(id) {
			var def = $q.defer();
			if ( artists.length > 0 ) {
				var result = _.find(artists, { id: parseInt(id) });

				def.resolve(result);
			}
			return def.promise;
		}

		function getFavoriteArtists() {
			if (artists.length > 0) {
				return artists.filter(function(artist) {
					return artist.favorite;
				});
			} else {
				return [];
			}
		}

		function setFavorite(id, favorite) {
			for (var i = 0; i < artists.length; i ++) {
				if (artists[i].id == id) {
					artists[i].favorite = favorite;
				}
			}
		}
	}	
})();