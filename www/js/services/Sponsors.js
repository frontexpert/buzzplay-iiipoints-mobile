(function () {
  'use strict';

  angular
  	.module('iiipoints.services')
		.service('SponsorsFactory', SponsorsFactory);

  SponsorsFactory.$inject = ['$q', '$http', 'APP_CONFIG'];
	function SponsorsFactory($q, $http, APP_CONFIG) {
		var artists = [];

		var service = {
			getSponsors: getSponsors
		};

		return service;

		function getSponsors() {
			var def = $q.defer();

			$http.get(APP_CONFIG.API_BASE_URL + 'sponsors')
				.then(function (response) {
					artists = response.data.data;
					def.resolve(response.data);
				}, function (error) {
					def.reject(error);
				});

			return def.promise;
		}
	}
		
	
})();