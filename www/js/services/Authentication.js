;(function () {
  'use strict'

  angular
    .module('iiipoints.services')
    .factory('Authentication', Authentication)

  Authentication.$inject = ['$q', '$http', 'APP_CONFIG', 'localStorageService']
  function Authentication ($q, $http, APP_CONFIG, localStorageService) {
    var token
    var userId

    var service = {
      createUser: createUser,
      login: login,
      logout: logout
    }

    return service

    // //////////////

    function createUser (userData) {
      var def = $q.defer()

      var params = {
        username: userData.username,
        password: userData.password,
        first_name: userData.firstName,
        last_name: userData.lastName,
        festival_id: APP_CONFIG.FESTIVAL_ID
      }

      $http({
        method: 'POST',
        url: APP_CONFIG.KINVEY_BASE_URL + 'user/' + APP_CONFIG.KINVEY_APP_KEY + '/',
        data: params,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + APP_CONFIG.KINVEY_AUTH,
          'X-Kinvey-API-Version': 3
        }
      })
        .success(function (res) {
          token = res._kmd.authtoken
          localStorageService.set('token', token)
          def.resolve(res)
        })
        .error(function (err) {
          def.reject(err)
        })

      return def.promise
    }

    function login (username, password) {
      var def = $q.defer()

      $http({
        method: 'POST',
        url: APP_CONFIG.KINVEY_BASE_URL + 'user/' + APP_CONFIG.KINVEY_APP_KEY + '/login',
        data: {
          username: username,
          password: password
        },
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + APP_CONFIG.KINVEY_AUTH,
          'X-Kinvey-API-Version': 3
        }
      })
        .success(function (response) {
          console.log('LOGIN:', response)
          token = response._kmd.authtoken
          userId = response._id
          localStorageService.set('token', token)
          localStorageService.set('userId', userId)
          def.resolve(response)
        })
        .error(function (err) {
          def.reject(err)
        })

      return def.promise
    }

    function logout () {
      var def = $q.defer()

      $http({
        method: 'POST',
        url: APP_CONFIG.KINVEY_BASE_URL + 'user/kid1781/_logout',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Basic ' + APP_CONFIG.KINVEY_AUTH,
          'X-Kinvey-API-Version': 3
        }
      })
        .success(function (res) {
          token = undefined
          localStorageService.remove('token')
          def.resolve(res)
        })
      error(function (err) {
        def.reject(err)
      })

      return def.promise
    }
  }
})()
