;(function () {
  'use strict'

  angular
    .module('iiipoints.services', [])
    .factory('FestopiaService', FestopiaService)

  FestopiaService.$inject = ['$q', '$http', 'APP_CONFIG', 'localStorageService']
  function FestopiaService ($q, $http, APP_CONFIG, localStorageService) {
    var schedule = [];
    var bands = [];
    var currentShowOnSchedule = false;

    var service = {
      getUserCredentials: getUserCredentials,
      saveUserCredentials: saveUserCredentials,
      checkToShowSettingsPopup: checkToShowSettingsPopup,
      setSeenLocationsSettings: setSeenLocationsSettings,
      setSeenNotificationsSettings: setSeenNotificationsSettings,
      setSeenConnectSettings: setSeenConnectSettings,
      regsiterDeviceForPush:regsiterDeviceForPush,
      fetchAllPOI: fetchAllPOI,
      fetchAllBands: fetchAllBands,
      getBand: getBand
    }

    return service

    // //////////////

    function getUserCredentials() {

      var username = localStorageService.get( 'username' );
      var userPassword = localStorageService.get( 'user_password' );
      if (username && userPassword) {
        return { username: username, password: userPassword };
      } else {
        return { username: 'FestopiaGuest', password: 'password' };
      }

    }

    function saveUserCredentials(username, password) {
      localStorageService.set('username', username);
      localStorageService.set('user_password', password);
    }

    function checkToShowSettingsPopup(key) {
      switch (key) {
        case 'SEEN_LOCATION_SETTINGS':
          return localStorageService.get('SEEN_LOCATION_SETTINGS') || false;
        case 'SEEN_NOTIFICATION_SETTINGS':
          return localStorageService.get('SEEN_NOTIFICATION_SETTINGS') || false;
        case 'SEEN_CONNECT_SETTINGS':
          return localStorageService.get('SEEN_CONNECT_SETTINGS') || false; 
      }
      return true;
    }

    function setSeenLocationsSettings() {
      localStorageService.set( 'SEEN_LOCATION_SETTINGS', true );
    }

    function setSeenNotificationsSettings() {
      localStorageService.set( 'SEEN_NOTIFICATION_SETTINGS', true );
    }

    function setSeenConnectSettings() {
      localStorageService.set( 'SEEN_CONNECT_SETTINGS', true );
    }

    function regsiterDeviceForPush(allow) {
      var def = $q.defer()
      
      UAirship.setUserNotificationsEnabled( allow, function (enabled) {
        console.log( 'Notifications enabled:', enabled );
        def.resolve();
      });

      return def.promise;
    }

    function fetchAllPOI() {
      var def = $q.defer();
      var query = '?query={"festival_id":"' + APP_CONFIG.FESTIVAL_ID + '"}';

      $http({
        method: 'GET',
        url: APP_CONFIG.KINVEY_BASE_URL + 'appdata/' + APP_CONFIG.KINVEY_APP_KEY + '/points-of-interest' + query,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Kinvey ' + localStorageService.get( 'token' ),
          'X-Kinvey-API-Version': 3
        }
      })
      .success(function(res) {
        def.resolve(res);
      })
      .error(function(err) {
        def.reject(err);
      });

      return def.promise;
    }

    function fetchAllBands() {
      var def = $q.defer();
      // var query = '?query={"festival_id":"58356130800bf6821f58c9d2"}';

      $http({
        method: 'GET',
        url: APP_CONFIG.KINVEY_BASE_URL + 'appdata/' + APP_CONFIG.KINVEY_APP_KEY + '/bands',// + query,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Kinvey ' + localStorageService.get( 'token' ),
          'X-Kinvey-API-Version': 3
        }
      })
      .success(function(res) {
        bands = res;
        def.resolve(res);
      })
      .error(function(err) {
        def.reject(err);
      });

      return def.promise;
    }

    function getBand(id) {
      var def = $q.defer();

      for(var i=0;i<bands.length;i++){
				if(bands[i]._id == id){
          def.resolve(bands[i]);
				}
			}
			
      return def.promise;
    }
  }
})()
