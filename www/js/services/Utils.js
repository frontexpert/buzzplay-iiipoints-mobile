;(function () {
  'use strict'

  angular
    .module('iiipoints.services')
    .factory('Utils', Utils)

  Utils.$inject = ['$ionicHistory', '$ionicLoading', '$ionicPopup']
  function Utils ($ionicHistory, $ionicLoading, $ionicPopup) {
    var service = {
      showLoading: showLoading,
      hideLoading: hideLoading,
      showAlert: showAlert,
      goBack: goBack
    };

    return service;

    // //////////////
    function showLoading () {
      $ionicLoading.show({
        template: '<p>Loading...</p><ion-spinner class="spinner-light" icon="lines"></ion-spinner>'
      });
    }

    function hideLoading () {
      $ionicLoading.hide();
    }

    function showAlert (caption, template) {
      console.log(caption);
      $ionicLoading.hide();
      return $ionicPopup.alert({
        title: caption,
        template: template
      });
    }

    function goBack () {
      $ionicHistory.goBack();
    }
  }
})();
