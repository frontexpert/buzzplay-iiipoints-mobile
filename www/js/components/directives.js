(function() {
  'use strict';

  angular
    .module('iiipoints.directives', [])
    .directive('actualSrc', ActualSrc);

  ActualSrc.$inject = [];
  function ActualSrc() {
    // Usage:
    //
    // Creates:
    //
    var directive = {
        link: link
    };
    return directive;

    function link(scope, element, attrs) {
      attrs.$observe('actualSrc', function(newVal, oldVal){
        if(newVal !== undefined){
          var img = new Image();
          if (attrs.actualSrc !== 'N/A' && img.src !== null && attrs.actualSrc !== 'null') {
            img.src = attrs.actualSrc;

            angular.element(img).bind('load', function () {
              element.attr("src", attrs.actualSrc);
            });
          }
        }
      });
    }
  }

  angular
    .module('iiipoints.directives')
    .directive('playerListener', function () {
      return function ($scope, $element) {
        $element[0].addEventListener("ended", function () {
          console.log('player stoped!');
          $scope.goHome();
        });
      }
    });

})();