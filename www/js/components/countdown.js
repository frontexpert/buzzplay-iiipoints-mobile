(function() {
  'use strict';

  angular
  	.module('iiipoints.directives')
  	.directive('countDown', CountDown);

  CountDown.$injector = [];
  function CountDown() {

  	var directive = {
  		restrict: 'E',
  		templateUrl: 'templates/components/countdown.html',
  		link: linkFn
  	};

  	return directive;

  	function linkFn(scope, element, attr) {
  		// Set the date we're counting down to
		var countDownDate = new Date("October 13, 2017 15:37:25").getTime();

		// Update the count down every 1 second
		var interval = setInterval(function() {
			// Get todays date and time
		    var now = new Date().getTime();
		    
		    // Find the distance between now an the count down date
		    var distance = countDownDate - now;
		    
		    scope.$apply(function () {
			  	// Time calculations for days, hours, minutes and seconds
			    scope.days = Math.floor(distance / (1000 * 60 * 60 * 24));
			    scope.hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			    scope.minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			});
		    

		    // If the count down is over, write some text 
		    if (distance < 0) {
		        clearInterval(interval);
		        // document.getElementById("demo").innerHTML = "EXPIRED";
		    }
		}, 1000);
  	}


  }
 })();