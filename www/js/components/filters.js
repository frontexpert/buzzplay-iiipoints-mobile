(function() {
  'use strict';

  angular
    .module('iiipoints.filters', [])
    .filter('formatUsTime', FormatUsTime);

  function FormatUsTime() {
    return FilterFilter;

    ////////////////

    function FilterFilter(Params) {
      var res = Params.split(':');
      var hours = res[0];
      var minutes = res[1];
      var ampm = hours >= 12 ? 'pm' : 'am';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      // minutes = minutes < 10 ? '0' + minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
    }
  }
})();