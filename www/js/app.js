// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('iiipoints', [
  'ionic',
  'plgn.ionic-segment',
  'iiipoints.controllers',
  'iiipoints.services',
  'iiipoints.directives',
  'iiipoints.filters',
  'ngCordova',
  'LocalStorageModule'
])

.run(function ($ionicPlatform) {
  $ionicPlatform.ready(function () {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true)
      cordova.plugins.Keyboard.disableScroll(true)
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleLightContent();
    }
  })
})
.constant('APP_CONFIG', {
  KINVEY_BASE_URL: 'https://baas.kinvey.com/',
  KINVEY_APP_KEY: 'kid_b1vnajEDkl',
  FESTIVAL_ID: '58356130800bf6821f58c9d2',
  KINVEY_AUTH: btoa('kid_b1vnajEDkl:10609ec172544ae6b75923af98bfab95'),
  MAP_API_KEY: 'AIzaSyDR2wqYMGUVgIncyCNtPYufz0JMGzJBMuc',
  API_BASE_URL: 'http://iiipoints.buzzplay.com/api/',
  MAP_IMAGE_URL: 'http://buzzplay.com/IIIpoints/iiipoints17-parkmap01.png'
})
.constant('APP_SOCIAL_LINKS', {
  BUY_TIX: 'https://iiipoints.frontgatetickets.com/?_ga=2.62294440.566426376.1502873751-786118215.1502250741',
  SNAPCHAT: 'https://www.snapchat.com/add/iiipoints',
  INSTAGRAM: 'https://www.instagram.com/iiipoints/',
  TWITTER: 'https://twitter.com/iiipoints',
  FACEBOOK: 'https://www.facebook.com/iiipoints',
  SOUNDCLOUD: 'https://soundcloud.com/iiipoints'
});
