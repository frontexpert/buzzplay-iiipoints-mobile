(function() {
'use strict';

  angular
    .module('iiipoints')
    .config(function($ionicConfigProvider, $cordovaInAppBrowserProvider, localStorageServiceProvider) {
      // $ionicConfigProvider.views.maxCache(5);
      // $ionicConfigProvider.views.transition('none');

      // note that you can also chain configs
      $ionicConfigProvider.backButton.previousTitleText(false);
      
      // Set default options for InAppBrowser window
      var defaultOptions = {
        location: 'no',
        clearcache: 'no',
        toolbar: 'no'
      };

      document.addEventListener("deviceready", function () {
        $cordovaInAppBrowserProvider.setDefaultOptions(defaultOptions)
      }, false);

      localStorageServiceProvider
        .setPrefix('iiipoints')
        .setNotify(true, true);

    });
})();